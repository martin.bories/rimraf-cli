image: node:lts

stages:
  - build
  - test
  - release

variables:
  GIT_SSL_NO_VERIFY: "true"
  GITLAB_TOKEN: $CI_JOB_TOKEN
  MEGATHERIUM_DATABASE_NAME: megatherium-test
  MEGATHERIUM_DATABASE_URL: mongodb://mongo:27017
  NPM_EMAIL: $MEGATHERIUM_NPM_EMAIL
  NPM_PASS: $MEGATHERIUM_NPM_PASSWORD
  NPM_TOKEN: $CI_JOB_TOKEN
  NPM_USER: $MEGATHERIUM_NPM_USERNAME

default:
  before_script:
    - npm i --cache .npm --prefer-offline
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - .npm/

build:
  stage: build
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - coverage
      - dist
      - docs
      - node_modules
      - src
      - package-lock.json
  script:
    - npm install
    - npm run build
  only:
    - master
    - merge_requests

test:
  stage: test
  needs:
    - build
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - coverage
      - dist
      - docs
      - node_modules
      - src
      - package-lock.json
  services:
    - mongo:latest
  script:
    # Setup npm registry
    - git config --global user.email "$MEGATHERIUM_EMAIL"
    - git config --global user.name "$MEGATHERIUM_NAME"
    - mkdir -p ~/.ssh
    - ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
    # Run tests
    - npm run test
  rules:
    - if: '($CI_COMMIT_AUTHOR !~ /semantic-release-bot/ || $CI_COMMIT_BRANCH != "master") && $CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: '($CI_COMMIT_AUTHOR =~ /semantic-release-bot/ && $CI_COMMIT_BRANCH == "master") || $CI_PIPELINE_SOURCE == "merge_request_event"'

release:
  stage: release
  needs:
    - build
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - coverage
      - dist
      - docs
      - node_modules
      - src
      - package-lock.json
  script:
    - npm run release
  artifacts:
    paths:
      - public
  interruptible: true
  rules:
    - if: '$CI_COMMIT_AUTHOR =~ /semantic-release-bot/ || $CI_COMMIT_BRANCH != "master"'
      when: never
    - if: '$CI_COMMIT_AUTHOR !~ /semantic-release-bot/ && $CI_COMMIT_BRANCH == "master"'


pages:
  stage: release
  needs:
    - build
    - test
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - coverage
      - dist
      - docs
      - node_modules
      - src
      - package-lock.json
  script:
    - mkdir .public
    - mkdir -p .public/coverage
    - cd coverage
    - cp -r * ../.public/coverage
    - cd ..
    - cd docs
    - cp -r * ../.public/
    - cd ..
    - mv .public public
    - npm run update
  artifacts:
    paths:
      - public
  interruptible: true
  rules:
    - if: '$CI_COMMIT_AUTHOR =~ /semantic-release-bot/ && $CI_COMMIT_BRANCH == "master"'
